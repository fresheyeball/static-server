Have a dockerized static file server with as little cruft as possible. It statically serves whatever it finds in the `/work` directory.

## Example usage

```
echo "serving current directory at localhost:10123"
docker run -v ${PWD}:/work -p 10123:80 registry.gitlab.com/fresheyeball/static-server:latest
```